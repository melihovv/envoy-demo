@include('vendor/autoload.php')

@servers(['local' => '127.0.0.1', 'remote' => 'root@207.154.198.110'])

@setup
    $userAndServer = 'root@207.154.198.110';
    $repository = "https://bitbucket.org/melihovv/envoy-demo";
    $baseDir = "/var/www/envoy-demo";
    $releasesDir = "{$baseDir}/releases";
    $currentDir = "{$baseDir}/current";
    $newReleaseName = date('Ymd-His');
    $newReleaseDir = "{$releasesDir}/{$newReleaseName}";

    function logMessage($message)
    {
        return "echo '\033[32m" .$message. "\033[0m';\n";
    }
@endsetup

@story('deploy')
    cloneRepository
    runComposer
    runYarn
    generateAssets
    updateSymlinks
    optimizeInstallation
    backupDatabase
    migrateDatabase
    insertNewFragments
    blessNewRelease
    cleanOldReleases
@endstory

@task('cloneRepository', ['on' => 'remote'])
    {{ logMessage("Cloning Repository...") }}

    [ -d {{ $releasesDir }} ] || mkdir -p {{ $releasesDir }};
    cd {{ $releasesDir }};

    mkdir -p {{ $newReleaseDir }};

    git clone --depth 1 {{ $repository }} {{ $newReleaseName }}

    # Configure sparse checkout
    cd {{ $newReleaseDir }}
    git config core.sparsecheckout true
    echo "*" > .git/info/sparse-checkout
    echo "!storage" >> .git/info/sparse-checkout
    echo "!public/build" >> .git/info/sparse-checkout
    git read-tree -mu HEAD

    # Mark release
    cd {{ $newReleaseDir }}
    echo "{{ $newReleaseName }}" > public/release-name.txt
@endtask

@task('runComposer', ['on' => 'remote'])
    {{ logMessage("Running Composer...") }}

    cd {{ $newReleaseDir }};
    composer install --prefer-dist --no-scripts --no-dev --quiet --optimize-autoloader --no-interaction;
@endtask

@task('runYarn', ['on' => 'remote'])
    {{ logMessage("Running Yarn...") }}

    cd {{ $newReleaseDir }};
    {{--TODO: yarn config set ignore-engines true--}}
    {{--TODO: yarn--}}
@endtask

@task('generateAssets', ['on' => 'remote'])
    {{ logMessage("Generating Assets...") }}

    cd {{ $newReleaseDir }};
    {{--TODO: yarn run production--}}
@endtask

@task('updateSymlinks', ['on' => 'remote'])
    {{ logMessage("Updating Symlinks To Persistent Data...") }}

    # Remove the storage directory and replace with persistent data
    rm -rf {{ $newReleaseDir }}/storage;
    cd {{ $newReleaseDir }};
    ln -nfs {{ $baseDir }}/persistent/storage storage;

    # Import the environment config
    cd {{ $newReleaseDir }};
    ln -nfs {{ $baseDir }}/.env .env;
@endtask

@task('optimizeInstallation', ['on' => 'remote'])
    {{ logMessage("Optimizing Installation...") }}

    cd {{ $newReleaseDir }};
    php artisan clear-compiled;
    php artisan optimize;
@endtask

@task('backupDatabase', ['on' => 'remote'])
    {{ logMessage("Backing Up Database...") }}

    cd {{ $newReleaseDir }}
    {{--TODO: php artisan backup:run--}}
@endtask

@task('migrateDatabase', ['on' => 'remote'])
    {{ logMessage("Migrating Database...") }}

    cd {{ $newReleaseDir }};
    php artisan migrate --force;
@endtask

@task('blessNewRelease', ['on' => 'remote'])
    {{ logMessage("🙏  Blessing new release...") }}

    cd {{ $currentDir }}
    php artisan config:env-validator

    ln -nfs {{ $newReleaseDir }} {{ $currentDir }};

    cd {{ $newReleaseDir }}
    php artisan config:clear
    php artisan cache:clear
    php artisan config:cache

    sudo service php7.0-fpm restart
@endtask

@task('cleanOldReleases', ['on' => 'remote'])
    {{ logMessage("Cleaning Up Old Releases...") }}

    cd {{ $releasesDir }}

    # Delete all but the 5 most recent.
    ls -dt {{ $releasesDir }}/* | tail -n +6 | xargs -d "\n" rm -rf;
@endtask

